#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>

int main(void)
{
    string fullName;
    int len;

    fullName = GetString(); 
    len = strlen(fullName);
    
    printf("%c", toupper(fullName[0]));

    for (int i = 1; i < len; i++) {
        int code = (int) fullName[i];
        if (code == 32) {
            printf("%c", toupper(fullName[i + 1]));
        }
    }

    printf("\n");
}
