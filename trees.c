struct node
{
    int data;
    struct node* left;
    struct node * right;
};

bool search(node* root, int value)
{
    while (root != NULL)
    {
        if (root->data == value)
        {
            return true;
        }

        if (root->data > value)
        {
            root = root->left;
        }
        else
        {
            root = root->right;
        }
    }

    return false;
}