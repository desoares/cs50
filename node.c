#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int val;
    struct node* next;
} node;

node* head = NULL;

void insertAfterThird(int toInsert)
{
    if (head == NULL) 
    {
        printf("List empty, no 3rd position to fill.");
        return 0;
    }
    
    node * crawler = head;
    
    for (int i = 0; i < 3; i++)
    {
        if (crawler->next == NULL)
        {
            printf("List not long enough. No 3rd position exits.\n");
            return 0;
        }
        
        crawler = crawler->next;
    }
    
    node* newNode = malloc(sizeof(node));
    
    if (newNode == NULL) 
    {
        printf("Out of heap memory!\n");
        return 1;
    }
    
    newNode->val = toInsert;
    newNode->next = crawler->next;
    crawler->next = newNode;
}
 
void printLast()
{
    if (head == NULL)
    {
        printf("List empty. No last elenet.\n");
        return 0;
    } 
    
    node* crawler = head;
    
    while (crawler->next != NULL) 
    {
        crawler = crawler->next;
    }
    
    pritnf("The last element is %d\n", crawler->val);
}
