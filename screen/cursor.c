#include <spl/gevents.h>
#include <spl/gobjects.h>
#include <spl/gwindow.h>

int main(void)
{
    GWindow window = newGWindow(360, 260);
    
    GOval circle = newGOval(0, 0, 50, 50);
    
    add(window, circle);
    
    while(1) {

        GEvent event = getNextEvent(MOUSE_EVENT);

        if (event != NULL) {

            if (getEventType(event) == MOUSE_MOVED) {
                int diam = getWidth(circle);
                double x = getX(event) -  diam / 2,
                       y = getY(event) - diam;
                setLocation(circle, x, y);
            }
        }
    }
    return 0;
}
