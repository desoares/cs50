#include <stdio.h>
#include <string.h>
#include <spl/gevents.h>
#include <spl/ginteractors.h>
#include <spl/gwindow.h>

int main(void)
{
    GWindow window = newGWindow(330, 220);
    
    GLabel left = newGLabel("0"),
           right = newGLabel("100");
    GSlider slider = newGSlider(0, 100, 50);
    
    setActionCommand(slider, "slide");       
    addToRegion(window, left, "SOUTH");
    addToRegion(window, slider, "SOUTH");
    addToRegion(window, right, "SOUTH");
    
    while (1) {
        
        GActionEvent event = waitForEvent(ACTION_EVENT);
        
        if (getEventType(event) == WINDOW_CLOSED) {
            break;
        }
        
        if (strcmp(getActionCommand(event), "slide") == 0) {
            printf("slider was slided to %i\n", getValue(slider));
        }
        
    }
    
    closeGWindow(window);
    return 0;
}
