#include <spl/gevents.h>
#include <spl/gobjects.h>
#include <spl/gwindow.h>

int main(void) 
{
    GWindow window = newGWindow(560, 480);
    double speed = 2.0;

    GOval circle = newGOval(0, 110, 20, 20);
    setColor(circle, "BLACK");
    setFilled(circle, true);
    add(window, circle);

    while(1) {

        move(circle, speed, 0);
        
        if (getX(circle) + getWidth(circle) >= getWidth(window) || getX(circle) <= 0) {
            speed = -speed;
        }

        pause(10);
    }    
}
