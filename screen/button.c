#include <stdio.h>
#include <string.h>
#include <spl/gevents.h>
#include <spl/ginteractors.h>
#include <spl/gwindow.h>

int main(void) 
{
    GWindow window =  newGWindow(320, 200);
    GButton but = newGButton("Click-me");
    
    setActionCommand(but, "click");
    addToRegion(window, but, "SOUTH");

    while(1) {
        GActionEvent event = waitForEvent(ACTION_EVENT);
        
        if (getEventType(event) == WINDOW_CLOSED) {
            break;
        }
        
        if (strcmp(getActionCommand(event), "click") == 0) {
            printf("Button was clicked\n");
        }
    }

    closeGWindow(window);
    return 0;
}
