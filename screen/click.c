#include <spl/gevents.h>
#include <spl/gwindow.h>

int main(void)
{
    GWindow windows = newGWindow(360, 260);
    
    while (true) {
        GEvent event = getNextEvent(MOUSE_EVENT);

        if (event != NULL) {
            if (getEventType(event) == MOUSE_CLICKED) {
                printf("%.0f x %.0f\n", getX(event), getY(event));
            }
        }
    }
    
    return 0;
}
