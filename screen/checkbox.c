#include <stdio.h>
#include <string.h>
#include <spl/gevents.h>
#include <spl/ginteractors.h>
#include <spl/gwindow.h>

int main(void)
{
    GWindow window = newGWindow(320, 240);

    GCheckBox chkbox = newGCheckBox("I agree");
    setActionCommand(chkbox, "chk");
    addToRegion(window, chkbox, "SOUTH");

    while (1) {
        GActionEvent event = waitForEvent(ACTION_EVENT);
        
        if (getEventType(event) == WINDOW_CLOSED) {
            break;
        }

        if (strcmp(getActionCommand(event), "chk") == 0) {

            if (isSelected(chkbox)) {

                printf("checkbox was checked\n");

            } else {

                printf("checkbox was unchecked\n");

            }
        }
    }

    closeGWindow(window);
    return 0;
}
