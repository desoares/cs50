#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>

int main(int argc, string argv[])
{
    int buffer,
        len,
        keyLen,
        i,
        j;

    string crky;

    char* text,
        letter;

    if (argc != 2) {
        printf("Only the alphabetical cypher key as parameter, please.\n");
        return 1;
    }
    
    crky = argv[1];
    keyLen = strlen(crky);
    int convKey[keyLen];

    for (i =  0; i < keyLen; i++) {
        letter = toupper(crky[i]);

        if (letter < 'A' || letter > 'Z') {
            printf("Use only the alphabetical characters in the cypher key.\n");
            return 1;
        }
        
        convKey[i] = (int) letter - 65;
    }    
    
    text = GetString();
    len = strlen(text);
    j = 0;

    if (text != NULL) {

        for (i = 0; i < len; i++) {

            int index = j % keyLen;

            if ((text[i] > 'Z' && text[i] < 'a') || text[i] < 'A' || text[i] > 'z') {

                printf("%c", text[i]);

            } else {

                buffer = text[i] + convKey[index];

                if ((buffer > 'Z' && text[i] < 'a') || buffer > 'z') {

                    buffer -= (convKey[index]);
                    buffer -= (26 - convKey[index]);

                }

                printf("%c", buffer);
                j++;
            }
        }
    }

    printf("\n");
    return 0;
}
