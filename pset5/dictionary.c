/****************************************************************************
 * dictionary.c
 *
 * Computer Science 50
 * Problem Set 5
 *
 * Implements a dictionary's functionality.
 ***************************************************************************/

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "dictionary.h"

/**
 * Returns true if word is in dictionary else false.
 */
bool check(const char* word)
{
    // TODO
    return false;
}

/**
 * Loads dictionary into memory.  Returns true if successful else false.
 */
bool load(const char* dictionary)
{//8:50
    FILE* dic = fopen(dictionary, "r");
    char letter;
    node* root = rootIni();

    while (fread(&letter, sizeof(char), 1, dic))
    {
    	int index = letter - 'a';

    	if (root->children == NULL) //new word
    	{
    		node *newNode = NULL;
    		newNode = (node *)malloc(NODE_SIZE);
    		root->children[index] = newNode;
    	}

    	if (root->children[index] == NULL)
    	{
    		root->children[index] = rootIni();
    	}
    	root = root->children[index];

    	if (letter == '\n')
    	{
    		root->isWord = true;
    	}
    	else
    	{
    		root->isWord = false;
    	}
    }

    return true;
}

/**
 * Returns number of words in dictionary if loaded else 0 if not yet loaded.
 */
unsigned int size(void)
{
    // TODO
    return 0;
}

/**
 * Unloads dictionary from memory.  Returns true if successful else false.
 */
bool unload(void)
{
    // TODO
    return false;
}

node *rootIni(void)
{
	node *root = NULL;
	root = (node *)malloc(NODE_SIZE);
	int i = 26;

	while (i)
	{
		root->children[i] = NULL;
		i--;
	}

	return root;
}