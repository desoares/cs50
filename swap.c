#include <stdio.h>
#include <cs50.h>

void swap(int* a, int* b);

int main(void)
{
    printf("Type a values: ");
    int x = GetInt();
    printf("Type a different value: ");
    int y = GetInt();
    printf("Swapping...\n");
    swap(&x, &y);
    printf("Swapped!\n");
    printf("x is %i\n", x);
    printf("y is %i\n", y);
}

void swap(int* a, int* b)
{
    int tmp = *a;
    
    *a = *b;
    *b = tmp;
}
