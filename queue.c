typedef struct
{
    int head;
    int length;
    int elements[CAPACITY];
}
queue;

/*
 * dequeue() - remove an element from the head of hte queue
 *
 * @param queue* the queue to manipulate
 * @param int* element - a pointer to a variable to hold the element
 * @returns bool - true if removal was successful
 */

bool dequeue(queue* q, int* element)
{
    if (q->length > 0)
    {
        *element = q->elements[q->head];
        q->head = (q->head + 1) % CAPACITY;
        q->length--;
        printf("dequeue(): %d\n", element);
        return true;
    }

    return false;
}

/*
 * enqueue() - add an element to the queue
 *
 * @param queue* the queue to manipulate
 * @param int element - the element to be added
 * @returns bool - true if addition was successful
 */

bool enqueue(queue* q, int element)
{
    if (q->length < CAPACITY)
    {
        q->elements[(q->head + q->length) % CAPACITY] = element;
        q->length++;
        printf("enqueue(): %d\n", element);

        return true;
    }

    return false;
}

/*
 * isempty() - cheks if the queue is empty
 *
 * @param queue* the queue to manipulate
 * @returns bool - true if queue is empty
 */

 bool is empty(queue* q)
 {
    return q->length == 0;
 }