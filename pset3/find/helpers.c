/**
 * helpers.c
 *
 * Computer Science 50
 * Problem Set 3
 *
 * Helper functions for Problem Set 3.
 */
       
#include <cs50.h>
#include <stdio.h>
#include "helpers.h"

/**
 * Returns true if value is in array of n values, else false.
 */
bool search(int value, int values[], int n)
{
//linear search
/*
    for (int i = 0; i < n; i++) {
        if (values[i] == value) {
            return true;
        }
    } 
*/
//Binary search
    while (n > 0) {
        int testIndex = n / 2;     

        if (n % 2 == 1 && testIndex != 0) {
            testIndex += 1;
        }

        int tested = values[testIndex];

        if (tested == value) {
            return true;
        }

        int newArr[testIndex];
        
        if (tested > value) {

            for (int i = 0; i < testIndex; i++) {
                newArr[i] = values[i];
            }  
            return search(value, newArr, testIndex);
        }
        
        if (tested < value) {
            
            for (int i = 0; i < testIndex; i++) {
                newArr[i] = values[testIndex + i];
            }
            return search(value, newArr, testIndex);
        }
        
    }
        
    return false;
}

/**
 * Sorts array of n values.
 */
void sort(int values[], int n)
{
    int buffer;

    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (values[i] >  values[j]) {
                buffer = values[i];
                values[i] = values[j];
                values[j] = buffer;

            }
        }
    }

    return;
}
