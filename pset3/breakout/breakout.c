//
// breakout.c
//
// Computer Science 50
// Problem Set 3
//

// standard libraries
#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Stanford Portable Library
#include <spl/gevents.h>
#include <spl/gobjects.h>
#include <spl/gwindow.h>

// height and width of game's window in pixels
#define HEIGHT 600
#define WIDTH 400

// number of rows of bricks
#define ROWS 5

// number of columns of bricks
#define COLS 10

// radius of ball in pixels
#define RADIUS 10

// lives
#define LIVES 3

// paddle width
#define PADDLE_X 60

// paddle height
#define PADDLE_Y 10

// set width of the ball
#define DIAM RADIUS * 2

// set limit to drand
#define LIMIT 65536

// prototypes
void initBricks(GWindow window);
GOval initBall(GWindow window);
GRect initPaddle(GWindow window);
GLabel initScoreboard(GWindow window);
void updateScoreboard(GWindow window, GLabel label, int points);
GObject detectCollision(GWindow window, GOval ball);
int ricochet(int start);

int main(void)
{
    // seed pseudorandom number generator
    srand48(time(NULL));
 
    //genate first thrust
    int sX = ricochet(-1);
    int sY = 2;

    // instantiate window
    GWindow window = newGWindow(WIDTH, HEIGHT);

    // instantiate bricks
    initBricks(window);
    
    // instantiate scoreboard, centered in middle of window, just above ball
    GLabel label = initScoreboard(window);

    // instantiate ball, centered in middle of window
    GOval ball = initBall(window);

    // instantiate paddle, centered at bottom of window
    GRect paddle = initPaddle(window);

    // number of bricks initially
    int bricks = COLS * ROWS;

    // number of lives initially
    int lives = LIVES;

    // number of points initially
    int points = 0;
    
    // wait for click before starting
    waitForClick();
    
    // keep playing until game over
    while (lives > 0 && bricks > 0)
    {    

        move(ball, sX, sY);

        GObject shock = detectCollision(window, ball);
        
        if (shock == paddle) {
            sX = ricochet(sX);
            sY = -sY;
        }

        if (shock != NULL && strcmp(getType(shock), "GRect") == 0 && shock != paddle)
        {
            sX = ricochet(sX);
            sY = -sY;
            removeGWindow(window, shock);
            bricks -= 1;
            points += 1;
            updateScoreboard(window, label, points);
        }

        if (getX(ball) + DIAM >= WIDTH || getX(ball) <= 0)
        {
            sX = -sX;
        }

        if ((getY(ball) + DIAM) >= HEIGHT)
        {
            lives -= 1; 
            move(ball, 0, 0);

            if (lives > 0) {
                waitForClick();
                removeGWindow(window, ball);
                ball = initBall(window);
            }
            
        }

        if (getY(ball) <= 0)
        {
            sY = -sY;
        }

        GEvent mouse = getNextEvent(MOUSE_EVENT);
        
        if (mouse != NULL) {

            if (getEventType(mouse) == WINDOW_CLOSED) {
                closeGWindow(window);
                return 0;
            }

            if (getEventType(mouse) == MOUSE_MOVED) {

                double x = getX(mouse) - PADDLE_X / 2;

                if (x + PADDLE_X > WIDTH) {                
                    x = WIDTH - PADDLE_X;
                } 
                
                if (x < 0) {
                    x = 1.0;
                }
                setLocation(paddle, x, HEIGHT - PADDLE_X);
            }
        }

        pause(10);
    }

    // wait for click before exiting
    waitForClick();

    // game over
    closeGWindow(window);
    return 0;
}

/**
 * Initializes window with a grid of bricks.
 */
void initBricks(GWindow window)
{
    int x, y;
    string color;
    
    for (int i = 0; i < ROWS; i ++) {

        int iMod = i % 5,
            size = WIDTH / COLS - (COLS / 2); 

        switch (iMod) {
            case 0: color = "GREEN"; break;
            case 1: color = "GREEN"; break;
            case 2: color = "YELLOW"; break;
            case 3: color = "BLUE"; break;
            default: color = "BLACK";
        }

        y = 51 + (i * (10 + 5));

        for (int j = 0; j < COLS; j++) {
            x = 2 + (5 + size) * j;
            GRect brick = newGRect(x, y, size, 10);
            setColor(brick, color);

            if (strcmp(color, "BLACK") != 0) {
                setFilled(brick, true);
            }

            add(window, brick);
        }
    }
}

/**
 * Instantiates ball in center of window.  Returns ball.
 */
GOval initBall(GWindow window)
{
    GOval ball = newGOval(WIDTH / 2 - RADIUS, HEIGHT / 2 - RADIUS, DIAM, DIAM);
    setColor(ball, "BLACK");
    setFilled(ball, true);
    add(window, ball);
    return ball;
}

/**
 * Instantiates paddle in bottom-middle of window.
 */
GRect initPaddle(GWindow window)
{
    GRect paddle = newGRect(WIDTH / 2 - (PADDLE_X / 2), HEIGHT - PADDLE_X, PADDLE_X, PADDLE_Y);
    setColor(paddle, "BLACK");
    setFilled(paddle, true);
    add(window, paddle);
    return paddle;
}

/**
 * Instantiates, configures, and returns label for scoreboard.
 */
GLabel initScoreboard(GWindow window)
{
    GLabel scoreBoard = newGLabel("0");
    setFont(scoreBoard, "SansSerif-48");
    setColor(scoreBoard, "LIGHT_GRAY");
    add(window, scoreBoard);
    setLocation(scoreBoard, WIDTH / 2 - getWidth(scoreBoard) / 2 , HEIGHT / 2 - RADIUS - 18);
    
    return scoreBoard;
}

/**
 * Updates scoreboard's label, keeping it centered in window.
 */
void updateScoreboard(GWindow window, GLabel label, int points)
{
    // update label
    char s[12];
    sprintf(s, "%i", points);
    setLabel(label, s);

    // center label in window
    double x = WIDTH / 2 - getWidth(label) / 2;
    double y = HEIGHT / 2 - getHeight(label) / 2;
    setLocation(label, x, y);
    
    return;
}

/**
 * Detects whether ball has collided with some object in window
 * by checking the four corners of its bounding box (which are
 * outside the ball's GOval, and so the ball can't collide with
 * itself).  Returns object if so, else NULL.
 */
GObject detectCollision(GWindow window, GOval ball)
{
    // ball's location
    double x = getX(ball);
    double y = getY(ball);

    // for checking for collisions
    GObject object;

    // check for collision at ball's top-left corner
    object = getGObjectAt(window, x, y);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's top-right corner
    object = getGObjectAt(window, x + 2 * RADIUS, y);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's bottom-left corner
    object = getGObjectAt(window, x, y + 2 * RADIUS);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's bottom-right corner
    object = getGObjectAt(window, x + 2 * RADIUS, y + 2 * RADIUS);
    if (object != NULL)
    {
        return object;
    }

    // no collision
    return NULL;
}

int ricochet(int start)
{

    int numer = (int) (drand48() * LIMIT);

    if (start > 0) {
        start = -1;
    } else {
        start = 1;
    }

    return (numer % 3) * start;
}
