#include <stdio.h>
#include <cs50.h>

int main(void) 
{
    int rows;
    bool test;

    printf ("height:");

    do {

        rows = GetInt();
        test = (rows < 0 || rows > 23);
        
        if (test) {
            printf("height: %i\n", rows);
            printf("Retry:"); 
        }

    } while (test);

    for (int i = 0; i < rows; i++) { 

        int blanks = rows - i, total = rows + 1;

        while (total > 0) {

            if (blanks > 1) {
                printf(" ");
                blanks--;
            } else {
                printf("#");                
            }

            total--;
        }

        printf("\n");
    }
}
