#include <cs50.h>
#include <stdio.h> 


typedef struct
{
    string name;
    int id;
    int phont_number;
    string house;
}
student;

int main(int argc, char *argv[])
{
    student walker;

    walker.name = "Walker";
    walker.id = 65883626;
    walker.house = "Winthrop";
    
    int copy_id = walker.id;
    
    printf("%i\n", copy_id);

    return 0;
}
