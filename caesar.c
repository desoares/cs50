#include <cs50.h>
#include <stdio.h>
#include <string.h>

int main(int argc, string argv[])
{
    if (argc != 2) {
        printf("The program requires and only accepts the cypher as parameter.\n");
        return 1;
    }

    int crky = (int) atoi(argv[1]);

    if (crky < 1) {
        printf("The cypher must a number greater than zero.\n");
        return 1;
    }
    
    string text = GetString();
    int len = strlen(text),
        buffer;

    if (crky > 25) {
        crky = crky%26;
    }

    for (int i = 0; i < len; i++) {

        if ((text[i] > 'Z' && text[i] < 'a') || text[i] < 'A' || text[i] > 'z') {

            printf("%c", text[i]);            

        } else {

            buffer = text[i] + crky;
            
            if ((buffer > 'Z' && text[i] < 'a') || buffer > 'z') {
                buffer -= (crky);
                buffer -= (26-crky);
                
            }
            
            printf("%c", buffer);
        }
    }

    printf("\n");
    return 0;
}
