int strlen(char* s)
{
    if (s == NULL)
    {
        return 0;
    }
    
    int length = 0;
    
    for (char* i = s; *i != '\0'; i++) {
        length++;
    }
    
    return length;
}
