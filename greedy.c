#include <stdio.h>
#include <cs50.h>
#include <math.h>

int main(void)
{
    int quarters, dimes, nickels, pennies, cents, mod, coins;
    float change;
    bool test;
    
    printf("How much change is owed? ");
    
    do {
        change = GetFloat();
        test = (change <= 0);
        if (test) {
            printf("Retry: ");
        }
    } while (test);
    
    
    
    //printf("change: %f\n", change);    
    cents = round(change * 100);
    
    //printf("value in cents: %i\n", cents);
    
    quarters = cents/25;
    mod = cents%25;
    //printf("%i quarters, mod %i\n", quarters, mod);
    dimes = mod/10;
    mod = mod%10;
    //printf("%i dimes, mod %i\n", dimes, mod);
    nickels = mod/5;
    //printf("%i nickels, mod %i\n", nickels, mod);
    pennies = mod%5;
    //printf("%i pennies\n", pennies);
    coins = quarters + dimes + nickels + pennies;
    
    printf("%i\n", coins);
}
