/**
 * recover.c
 *
 * Computer Science 50
 * Problem Set 4
 *
 * Recovers JPEGs from a forensic image.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define RAW_FILE "card.raw"
#define SIZE 512

typedef uint8_t byte;


int main(int argc, char* argv[])
{
    FILE* card = fopen(RAW_FILE, "r");
    if (card == NULL)
    {
        printf("Could not find file.");
    }

    byte cluster[SIZE];
    int count = 0;
    char name[8];
    FILE* newFile = NULL;

    if (cluster != NULL)
    {
        while (fread(&cluster, SIZE, 1, card) != 0)
        {
            if (cluster[0] == 0xff && cluster[1] == 0xd8 && cluster[2] == 0xff &&
               (cluster[3] == 0xe0 || cluster[3] == 0xe1))
            {
                if (newFile != NULL)
                {
                    fclose(newFile);
                }
                sprintf(name, "%03d.jpg", count);
                newFile = fopen(name, "w");
                count ++;
           }

            if (newFile != NULL)
            {
                fwrite(&cluster, SIZE, 1, newFile);
            }
        }
    }

    fclose(card);
    return 0;
}