/**
 * resize.c
 *
 * Computer Science 50
 * Problem Set 4
 *
 * Resize a BMP n a give factor.
 */

#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"

#define  RGB_SIZE sizeof (RGBTRIPLE)

int getPadding(int width);

int main(int argc, char* argv[])
{
    // ensure proper usage
    if (argc != 4)
    {
        printf("Usage: ./resize n infile outfile\n");
        return 1;
    }

    //store filenames
    char* infile = argv[2];
    char* outfile = argv[3];
    //resize factor
    int factor = atoi(argv[1]);

    if  (factor < 1 || factor > 100)
    {
        printf("Factor must between 1 and 100.\n");
        return 1;
    }

    // open input file
    FILE* inptr = fopen(infile, "r");
    if (inptr == NULL)
    {
        printf("Could not open %s.\n", infile);
        return 2;
    }

    // open output file
    FILE* outptr = fopen(outfile, "w");
    if (outptr == NULL)
    {
        fclose(inptr);
        fprintf(stderr, "Could not create %s.\n", outfile);
        return 3;
    }

    // read infile's BITMAPFILEHEADER
    BITMAPFILEHEADER bf;
    fread(&bf, sizeof(BITMAPFILEHEADER), 1, inptr);

    // read infile's BITMAPINFOHEADER
    BITMAPINFOHEADER bi;
    fread(&bi, sizeof(BITMAPINFOHEADER), 1, inptr);

    // ensure infile is (likely) a 24-bit uncompressed BMP 4.0
    if (bf.bfType != 0x4d42 || bf.bfOffBits != 54 || bi.biSize != 40 ||
        bi.biBitCount != 24 || bi.biCompression != 0)
    {
        fclose(outptr);
        fclose(inptr);
        fprintf(stderr, "Unsupported file format.\n");
        return 4;
    }

    //store old values
    int oldWidth = bi.biWidth,
         oldHeight = bi.biHeight,
         oldPadding =  getPadding(oldWidth);

    //apply factor on img dimensions
    bi.biWidth *= factor;
    bi.biHeight *= factor;
    //new padding
    int padding =  getPadding(bi.biWidth);
    //update header
    bi.biSizeImage = ((bi.biWidth * RGB_SIZE) + padding) * abs(bi.biHeight);
    bf.bfSize = bi.biSizeImage + sizeof(BITMAPINFOHEADER) + sizeof(BITMAPFILEHEADER);

    // write outfile's headers
    fwrite(&bf, sizeof(BITMAPFILEHEADER), 1, outptr);
    fwrite(&bi, sizeof(BITMAPINFOHEADER), 1, outptr);

    //save scanlines
    //RGBTRIPLE *tripleBuff = malloc(RGB_SIZE * bi.biWidth);

    // iterate over infile's scanlines
    for (int i = 0, height = abs(oldHeight); i < height; i++)
    {
        //int counter = 0;
        // iterate over pixels in scanline
        for (int j = 0; j < oldWidth ; j++)
        {
            ///temporary storage
            RGBTRIPLE triple;
            // read RGB triple from infile
            fread(&triple, RGB_SIZE, 1, inptr);
            //thats's what I done:
            int count = factor;
            while (count) 
            {
                fwrite(&triple, RGB_SIZE, 1, outptr);
                count--;
            }
            // for (int k = 0;  k < factor; k++){
            //     write RGB triple to outfile
            //     *(tripleBuff+(counter)) = triple;
            //     counter++;
            // }
        }
        // skip over padding, if any
        fseek(inptr, oldPadding, SEEK_CUR);
//could not solve this found in internet
        // for (int y = 0; y < factor; y++) {
        //     fwrite((tripleBuff), RGB_SIZE, bi.biWidth, outptr);
        //     // then add it back (to demonstrate how)
        //     for (int m = 0; m < padding; m++)
        //     {
        //         fputc(0x00, outptr);
        //     }
        // }
    }

    //free buffer
    //free(tripleBuff);
    // close infile
    fclose(inptr);
    // close outfile
    fclose(outptr);

    // that's all folks
    return 0;
}

int getPadding(int width)
{
    return (4 - (width * RGB_SIZE) % 4) % 4;
}